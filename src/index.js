import mongoose from 'mongoose';
import express from 'express';
import morgan from 'morgan';
import { config } from 'dotenv';
import authRouter from './authRouter.js';
import notesRouter from './notesRouter.js';
import usersRouter from './usersRouter.js';
import authMiddleware from './middleware/authMiddleware.js';

config();

mongoose.connect('mongodb+srv://scumbrum:1@cluster0.0qde1n4.mongodb.net/?retryWrites=true&w=majority');

const server = express();
server.use(express.json());
server.use(morgan('tiny'));

server.use('/api/users', authMiddleware, usersRouter);
server.use('/api/auth', authRouter);
server.use('/api/notes', authMiddleware, notesRouter);

server.listen(process.env.PORT || 8080, () => {
  console.log(`Server start on port:${process.env.PORT || 8080}`);
});

function errorHandler(err, res) {
  res.status(500).send({ message: 'Server error' });
}

server.use(errorHandler);
