import Note from './schemas/Notes.js';

export const getNotes = async (req, res, next) => {
  const { limit, offset } = req.query;
  if (!limit || !offset) {
    return res.status(400).json({ message: 'Specify limit  and offset' });
  }
  try {
    const notes = await Note.find({ userId: req.user.id }).skip(offset).limit(limit);
    return res.json({
      limit,
      offset,
      count: notes.length,
      notes,
    });
  } catch (e) {
    return next();
  }
};

export const createNote = async (req, res, next) => {
  const { text } = req.body;
  if (!text) {
    return res.status(400).json({ message: 'Specify a text' });
  }
  try {
    const { id } = req.user;
    const note = new Note(
      {
        userId: id,
        createdDate: new Date().toJSON(),
        completed: false,
        text,
      },
    );
    await note.save();
    return res.json({ message: 'Success' });
  } catch (e) {
    return next();
  }
};

export const getNoteById = async (req, res, next) => {
  const { id } = req.params;
  try {
    const result = await Note.findOne({ _id: id, userId: req.user.id });
    return res.json(result);
  } catch (e) {
    return next();
  }
};

export const updateNoteById = async (req, res, next) => {
  const { id } = req.params;
  const { text } = req.body;
  if (!text) {
    return res.status(400).json({ message: 'Specify a text' });
  }
  try {
    await Note.updateOne({ _id: id, userId: req.user.id }, { text });
    return res.json({ messag: 'Success' });
  } catch (e) {
    return next();
  }
};

export const changeStatusOnNote = async (req, res, next) => {
  const { id } = req.params;
  try {
    const prev = await Note.findOne({ _id: id, userId: req.user.id });
    await Note.updateOne({ _id: id, userId: req.user.id }, { completed: !prev.completed });
    return res.json({ messag: 'Success' });
  } catch (e) {
    return next();
  }
};

export const deleteNoteById = async (req, res, next) => {
  const { id } = req.params;
  try {
    await Note.deleteOne({ _id: id, userId: req.user.id });
    return res.json({ message: 'Success' });
  } catch (e) {
    return next();
  }
};
