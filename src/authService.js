import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import User from './schemas/Users.js';

export const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  const exist = await User.findOne({ username: req.body.username });
  if (exist) {
    return res.status(400).json({ message: 'Exist username' });
  }
  try {
    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
      createdDate: new Date().toJSON(),
    });
    await user.save();
    return res.json({
      message: 'Success',
    });
  } catch (e) {
    return next();
  }
};

export const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, id: user._id };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.json({
      message: 'Success',
      token: jwtToken,
    });
  }
  return res.status(403).json({ message: 'Not authorized' });
};
