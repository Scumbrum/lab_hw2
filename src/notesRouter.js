import { Router } from 'express';
import {
  changeStatusOnNote,
  createNote, deleteNoteById, getNoteById, getNotes, updateNoteById,
} from './notesService.js';

const router = Router();

router.get('/', getNotes);

router.post('/', createNote);

router.get('/:id', getNoteById);

router.put('/:id', updateNoteById);

router.patch('/:id', changeStatusOnNote);

router.delete('/:id', deleteNoteById);

export default router;
