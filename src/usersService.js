import bcrypt from 'bcrypt';
import Note from './schemas/Notes.js';
import User from './schemas/Users.js';

export const getUserData = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.user.username });
    return res.json({ id: user._id, username: user.username, createdDate: user.createdDate });
  } catch (e) {
    return next(e.message);
  }
};

export const deleteUser = async (req, res, next) => {
  try {
    await User.deleteOne({ username: req.user.username });
    await Note.deleteMany({ userId: req.user.id });
    return res.json({ message: 'Success delete' });
  } catch (e) {
    return next(e.message);
  }
};

export const updateUser = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).json({ message: 'Specify passwords' });
  }
  try {
    const user = await User.findOne({ username: req.user.username });
    if (!user) {
      return res.status(400).json({ message: 'Not authorised' });
    }
    if (!await bcrypt.compare(String(oldPassword), String(user.password))) {
      return res.status(400).json({ message: 'Invalid data' });
    }
    await User.updateOne(
      { username: req.user.username },
      { password: await bcrypt.hash(newPassword, 10) },
    );
    return res.json({ message: 'Success update' });
  } catch (e) {
    return next(e.message);
  }
};
