import { Router } from 'express';
import { deleteUser, getUserData, updateUser } from './usersService.js';

const router = Router();

router.get('/me', getUserData);

router.patch('/me', updateUser);

router.delete('/me', deleteUser);

export default router;
